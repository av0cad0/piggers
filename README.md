README.md

Pig dice 

pig is a simple dice game
Players play by rolling dice and holding their points during the game


features-

    The game comes with a help menu where all commands available to userd are shown
    A "rules" menu is available for beginners where they can learn how the game works.

    The game allows the creation of accounts with passwords where players statistics such as
    - points
    - wins
    - losses
    are saved to a binary file.
    player names can be changed once the account is logged in

    The game holds a top 5 players list of players with highest top scores which is updated whenever a player beats any of the top 5 player top scores.

    The game has a cheat mode, when activated player will not roll a 1 until end of game.
    When player activates cheat mode their new top score will not be added to the list.

    It can be played in singular mode or versus a computer with three difficulty levels
    1. easy
    2. normal
    3. hard


Structure- 

    shell class:
        Takes command from users and executes corresponding method
    
    game class:
        The heart of the game
        Most of the intelligence and operations are done in game class.
        Player class intances are saved in a list in game class.
        Computer and Player class data are only returned to game class.
        Leaderboards class is updated through game class and data is only return to game class.
        Synchronization class is used from game class and only returns data to game class.
        Has a test class.

    player class:
        Instances of player class will be created for each payer created.
        Roll method is called from the player instances.
        all the methods in player class only return data, no prints
        Has a test class

    Computer class:
        Computer class only holds methods and variables for game class to access.
        The computer intelligence is explained in "Computer intelligence" lower down.
        Roll method is accessed through Computer class.
        Has a test class

    Dice class:
        Dice class holds a method of "roll" for computer and player class to access.
        The Dice class intelligence is explained in "Dice intelligence" lower down.
        Has a test class

    Leaderboards class:
        Holds a tuple "scoreboard" of top five highest scoring players.
        The scoreboard is updated every time a game ends.
        Has a test class

    Synchronization class:
        Updates leaderboards.pickle and users.pickle files with top players and the users.
        Only returns data to game class
        Has a test class


Dice intelligence-

    dice class holds multiple tuples of numbers for the different dice
    the normal die has 6 numbers, 1 to 6
    the easy die for the easy level computer has 10 integers majority ones and twos
    the hard die for the hard level computer has 10 integers majority fives and sixs
    the cheat die for when player activates cheat mode has 5 integers, 2 to 6

    the roll method in dice class is automated
    it takes a parameter of condiion which is either a diffculty level
    or a boolean for if the player is cheating
    this way only one menthod is needed to be called from the player and computer class

Computer intelligence-

    computer difficulty is saved into a variable
    everytime the computer executes its roll() method it directs its difficulty string as a parameter and rolls the correct die.

    computer class holds a variable of action_pool with a 35% chance of holding
    everytime the computer rolls the action_pool hold percentage increases by 5%
    until the computer holds their current points 
    and then the action_pool resets to 35%.


Install guide:

    Getting the game:
        In Gitbash
        In the chosen directory enter
        -- git clone https://gitlab.com/av0cad0/piggers.git

    to get update:
        In Gitbash
        -- git pull

    To launch game enter:
        In Gitbash/CMD
        -- pig.py
        or
        -- main.py
    

Run guide:

    for cmd
    -- pig.py
    or 
    -- main.py
    
    for gitbash
    -- py/python/python3 pig.py
    or 
    -- py/python/python3 main.py


Document guide:

    setup:
        -- make venv
        -- cd .venv/scripts
        -- . activate
        -- cd ../..
        -- make install

    Automated uml and documentation:
        -- make doc

    Quick read pydoc:
        -- py/python/python3 -m pydoc <filename>

    Generate html page pydoc:
        -- py/python/python3 -m pydoc -w <filename>

    Generate html documentation:
        -- make pdoc

    Generate uml:
        -- pyreverse <filename>
        -- dot -Tpng classes.dot -o classes.png
        or
        -- make pyreverse


Test guide-

    Execute all unittests:
        -- make unittest
        or
        -- py/python/python3 -m unittest discover . "*_test.py"
        or
        # for more details
        -- py/python/python3 -m unittest discover -v . "*_test.py"
    

Coverage guide-

    Automated:
        -- make coverage

    Run coverage report:
        -- coverage run -m unittest discover . "*_test.py"

    Execute coverage report:
        -- coverage report -m

    Execute coverage and generate html:
        -- coverage html


Play through video:

    https://www.youtube.com/watch?v=Ja1D500e3Jo


Support-

    For asking questions regardig the game contact these emails:
    - sam.hurenkamp0036@stud.hkr.se
    - ramin.darudi0013@stud.hkr.se


Built with-

    Visual studio code


Team-

    - Sam Hurenkamp
    - Ramin Darudi
    
Releases-

    V1: 2021-02-12