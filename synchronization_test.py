"""Importing required modules for class to work."""
import unittest
import synchronization
import leaderboards
import os
import game


class SynchronizationTest(unittest.TestCase):
    """Testing that file reading and writing is handled correctly."""

    directory = "data/testing"
    f1 = "data/testing/test_users.pickle"
    f2 = "data/testing/test_leaderboards.pickle"

    def setUp(self):
        """Create variables for tests."""
        self.sync = synchronization.Sync()
        self.game = game.Game()
        self.leaderboards = leaderboards.Leaderboards()
        self.leaderboards.scoreboard = leaderboards.Leaderboards.scoreboard
        self.sync.file1 = self.f1
        self.sync.file2 = self.f2
        self.player1 = self.game.create_player("John test")
        self.player2 = self.game.create_player("Jane test")
        os.mkdir(self.directory)

    def tearDown(self):
        """Tear down testing variables."""
        if os.path.exists(self.f1):
            os.remove(self.f1)
        if os.path.exists(self.f2):
            os.remove(self.f2)
        if os.path.exists(self.directory):
            os.rmdir(self.directory)
        self.game.list_players.clear()

    def test_self_write(self):
        """Test that writing happens correctly and a file is generated."""
        self.assertFalse(os.path.exists(self.f1))
        self.assertFalse(os.path.exists(self.f2))
        self.sync.pickle_write()
        self.assertTrue(os.path.exists(self.f1))
        self.assertTrue(os.path.exists(self.f2))

    def test_self_read(self):
        """Test that the file is read from correctly."""
        self.assertTrue(self.sync.file1, self.f1)
        self.assertTrue(self.sync.file2, self.f2)
        self.assertTrue(os.path.exists(self.directory))
        """Ensure files exists for reading."""
        if (os.path.exists(self.f1) and os.path.exists(self.f2)) is False:
            self.test_self_write()
        self.assertTrue(os.path.exists(self.f1))
        self.assertTrue(os.path.exists(self.f2))

        """Ensure contents of files are empty/contain created players."""
        self.sync.pickle_read()
        self.assertEqual(len(self.game.list_players), 2)
        self.assertEqual(len(self.leaderboards.scoreboard), 5)
        for i in range(0, len(self.leaderboards.scoreboard)):
            self.assertEqual(self.leaderboards.scoreboard[i].username, "")
