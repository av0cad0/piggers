"""Test the computer class to ensure functionality."""
import unittest
import computer
import dice


class TestComputerClass(unittest.TestCase):
    """Test class for computer class in pig."""

    def test_username(self):
        """Test that the username is final and as expected."""
        self.assertTrue(computer.Computer.username == "Computer")

    def test_difficuly_string(self):
        """This test will check whether difficulty of type String changes."""
        self.assertEqual(computer.Computer.difficulty, "")
        computer.Computer.difficulty = "easy"
        self.assertEqual(computer.Computer.difficulty, "easy")

    def test_points_held(self):
        """This test checks whther points_held gets updated correctly."""
        computer.Computer.points_held = 100
        self.assertEqual(computer.Computer.points_held, 100)

    def test_action_pool(self):
        """Testing variable action_pool."""
        exp = list(computer.Computer.final_action_pool)
        res = computer.Computer.action_pool
        self.assertEqual(res, exp)

    def test_times_rolled(self):
        """Testing times_rooled."""
        exp = 0
        res = computer.Computer.times_rolled
        self.assertEqual(res, exp)

    def test_final_action_pool(self):
        """Testing variable final_action_pool."""
        exp = (
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "roll",
            "hold",
            "hold",
            "hold",
            "hold",
            "hold",
            "hold",
            "hold"
        )
        res = computer.Computer.final_action_pool
        self.assertEqual(res, exp)

    def test_reset_action_pool(self):
        """Testing method rese_action_pool."""
        exp = list(computer.Computer.final_action_pool)

        computer.Computer.action_pool[0] = "hold"
        self.assertEqual(computer.Computer.action_pool[0], "hold")

        computer.Computer.reset_action_pool(computer.Computer)

        self.assertEqual(computer.Computer.action_pool, exp)
        self.assertEqual(computer.Computer.action_pool[0], "roll")

    def test_roll(self):
        """Testing make_roll method."""
        easy = dice.Dice.dice_pool_easy
        normal = dice.Dice.dice_pool_normal
        hard = dice.Dice.dice_pool_hard

        rolling = computer.Computer().roll()

        computer.Computer.difficulty = "easy"
        self.assertTrue(rolling in easy)
        computer.Computer.difficulty = "normal"
        self.assertTrue(rolling in normal)
        computer.Computer.difficulty = "hard"
        self.assertTrue(rolling in hard)
