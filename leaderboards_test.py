"""Testing for scoreboard properties and behaviours."""

import unittest
import leaderboards
import player


class TestLeaderboards(unittest.TestCase):
    """Test class for leaderboards."""

    def setUp(self):
        """Create five mock scores for scoreboard."""
        self.leaderboards = leaderboards.Leaderboards()
        self.p1 = player.Player("a", "test")
        self.p2 = player.Player("b", "test")
        self.p3 = player.Player("c", "test")
        self.p4 = player.Player("d", "test")
        self.p5 = player.Player("e", "test")

        """ score testing """
        self.s1 = self.p1.points_held = 180
        self.s2 = self.p2.points_held = 170
        self.s3 = self.p3.points_held = 160
        self.s4 = self.p4.points_held = 150
        self.s5 = self.p5.points_held = 140

    def tearDown(self) -> None:
        """Teardown test."""
        return super().tearDown()

    def test_scoreboard(self):
        """Test scoreboard variable."""
        self.assertEqual(self.leaderboards.scoreboard, (
            player.Player,
            player.Player,
            player.Player,
            player.Player,
            player.Player
        ))

        for i in self.leaderboards.scoreboard:
            self.assertEqual(i.points_held, 0)

    def test_update(self):
        """Score testing."""
        self.assertEqual(
            (self.s1, self.s2, self.s3, self.s4, self.s5),
            (180, 170, 160, 150, 140)
            )

        """ initial name testing """
        n1 = self.p1.username
        n2 = self.p2.username
        n3 = self.p3.username
        n4 = self.p4.username
        n5 = self.p5.username
        self.assertEqual((n1, n2, n3, n4, n5), ("a", "b", "c", "d", "e"))

        """ updating empty scoreboard with previous mock objects """
        self.leaderboards.update(self.p1)
        exp = (
            self.p1, player.Player, player.Player, player.Player, player.Player
            )
        self.assertEqual(self.leaderboards.scoreboard, exp)

        self.leaderboards.update(self.p2)
        exp = (self.p1, self.p2, player.Player, player.Player, player.Player)
        self.assertEqual(self.leaderboards.scoreboard, exp)

        self.leaderboards.update(self.p3)
        exp = (self.p1, self.p2, self.p3, player.Player, player.Player)
        self.assertEqual(self.leaderboards.scoreboard, exp)

        self.leaderboards.update(self.p4)
        exp = (self.p1, self.p2, self.p3, self.p4, player.Player)
        self.assertEqual(self.leaderboards.scoreboard, exp)

        self.leaderboards.update(self.p5)
        exp = (self.p1, self.p2, self.p3, self.p4, self.p5)
        self.assertEqual(self.leaderboards.scoreboard, exp)

        """ testing for no new highscore """
        self.p6 = player.Player("f", "test")
        self.p6.points_held = 130
        self.leaderboards.update(self.p6)
        self.assertEqual(
            self.leaderboards.scoreboard,
            (self.p1, self.p2, self.p3, self.p4, self.p5)
            )

        """ testing for new highscore """
        self.p7 = player.Player("g", "test")
        self.p7.points_held = 165

        self.leaderboards.update(self.p7)

        self.assertEqual(
            self.leaderboards.scoreboard,
            (self.p1, self.p2, self.p7, self.p3, self.p4)
            )
        self.leaderboards.scoreboard = ()
